import React, { Component } from 'react';
// import ReactDOM from 'react-dom';
import { BrowserRouter, HashRouter, Route, Redirect, Switch } from 'react-router-dom';
import './App.css';
import HomePage from './components/HomePage'
import Albums from './components/Albums'
import Album from './components/Album'
import NoFound from './components/NoFound'
class App extends Component {
    constructor(props){
        super(props);
        this.state={
            artist:[]

        }
    }

    async componentDidMount(){
        const url = 'http://www.json-generator.com/api/json/get/cuKKbBWWXm?indent=2';
        fetch(url)
            .then(data => data.json())
            .then(data => {
                this.setState({
                    artist: data
                })
            })
            .catch(err => {
                console.log(err)
            });



        // async function fetchAsync () {

        // let response = await fetch(url);
        // let data = await response.json();
        // this.setState({
        //     artist: data
        // })
        // }
        // fetchAsync()
        //     .then(data =>
        //         this.setState({
        //             artist:data
        //         })
        //     )
        //     .catch(reason => console.log(reason.message));
    }
  render() {
    return (
      <div className="App">
          <BrowserRouter basename="/">
              <Switch>
                  <Route path="/" exact render={() => <HomePage  data={this.state.artist}/>}/>
                  <Route exact path="/albums/:albumId" render={(match) => <Albums match={match}  data={this.state.artist} />}/>
                  <Route exact path="/albums/:albumId/:album" render={(match) => <Album match={match}  data={this.state.artist} />}/>
                  <Route component={NoFound}/>
              </Switch>
          </BrowserRouter>
      </div>
    );
  }
}

export default App;
