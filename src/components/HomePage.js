import React from 'react';
import { Link, NavLink } from 'react-router-dom';


class HomePage extends React.Component{

    render(){
        const {data}=this.props;
        return(
            <div className="navigation">
                <ul>
                    {
                        data.map((item, index) =>
                            <li key={index}><Link to={"/albums/" + item.index}>{item.name}</Link></li>
                        )
                    }
                </ul>
            </div>
        )

    }
};


export default HomePage;