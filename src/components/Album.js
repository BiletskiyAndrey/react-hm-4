import React from 'react';
import { Link, NavLink } from 'react-router-dom';

const Album = ({match,data}) =>{
    const compId = match.match.params.albumId;
    const comp = match.match.params.album;
    let albums = [];
    data.map(item => {
        if(item.index === parseInt(compId)){

            albums = item.album

        }
    });
    return(
        <ul>
            {
                albums.map(item => {
                    if(item.name.toLowerCase() === comp){
                        return item.compositions.map((item,index)=> <li key={index}>{item.name + " - " + item.duration}</li>)
                        }}

                )

            }
        </ul>
    )
};

export default Album